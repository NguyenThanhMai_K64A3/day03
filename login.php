<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <div class="main">
        <form action="">
            <div class="title">
                <label class="label" for="username">Tên đăng nhập</label><br>
                <div><input class="usename" type="text" id="username" name="username"><br></div>
            </div>

            <div class="title">
                <label class="label">Giới tính</label><br>
                <div class="gender">
                    <?php
                        $gender = array('Nam'=>'Nam', 'Nu'=>'Nữ');
                        foreach($gender as $key => $value):
                            echo '<div class="gender-option"><input type="radio" value="'.$key.'value="0" name="Gender"><p>'.$value.'</p></div>';
                        endforeach;
                    ?>
                </div>
            </div>

            <div class="title">
                <label class="label" for="khoa">Phân khoa</label><br>
                <select name="khoa" id="khoa">
                    <option value=""></option>
                    <?php
                        $khoa = array('khmt'=>'Khoa học máy tính', 'khvl'=>'Khoa học vật liệu');
                        foreach($khoa as $key => $value):
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        endforeach;
                    ?>
                </select>
            </div>

            <button>Đăng ký</button>
        </form>
    </div>
</body>
</html>

